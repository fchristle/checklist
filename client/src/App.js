import React, { Component } from "react";
import ChecklistContainer from "./app/ChecklistContainer";
import Checklist from "./app/Checklist";
import ChecklistEditor from "./app/ChecklistEditor";
import ChecklistItemDetail from "./app/ChecklistItemDetail";
import "./App.css";

class App extends Component {
  state = { show: "list", id: null };

  updateShow = (show, checklistId, itemId) => {
    this.setState({ show, checklistId, itemId });
  };
  render() {
    let showComponent = null;
    const { show, checklistId, itemId } = this.state;
    switch (show) {
      case "list":
        showComponent = <ChecklistContainer updateShow={this.updateShow} />;
        break;
      case "view":
        showComponent = <Checklist checklistId={checklistId} updateShow={this.updateShow}/>;
        break;
      case "create":
        showComponent = <ChecklistEditor updateShow={this.updateShow} />;
        break;
      case "itemDetails":
        showComponent = <ChecklistItemDetail checklistId={checklistId} itemId={itemId} updateShow={this.updateShow} />;
        break;
      default:
        showComponent = <ChecklistContainer updateShow={this.updateShow} />;
        break;
    }
    return (
      <div className = "app">
        {showComponent}
      </div>
    );
  }
}

export default App;
