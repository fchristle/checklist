import React, {Component} from 'react';
import {getChecklist, updateItem} from "./api-utils";

export default class ChecklistItemDetail extends Component {
  state = {item : null
          , text: null
          , nameEditMode: false
          , descriptionEditMode: false
          , description: null};

  componentDidMount() {
      const {checklistId, itemId} = this.props;
      getChecklist(checklistId).then(checklist => {
        const item = checklist.items.find(item => item._id === itemId);
        this.setState({item, text: item.text, description: item.description})
      });
  }

  backToChecklist = () => {
    this.props.updateShow("view", this.props.checklistId);
  }

  handleNameEdit = () => {
    this.setState({nameEditMode : !this.state.nameEditMode})
  }

  handleNameChange = (e) => {
    this.setState({text : e.target.value});
  }

  handleNameKeyPress = (e) => {
    const {item, text} = this.state;
    const {checklistId, itemId} = this.props;
    if (e.key === "Enter") {
      const newItem = Object.assign({...item, text});
      updateItem(checklistId, itemId, newItem).then(checklist => {
        const item = checklist.items.find(item => item._id === itemId);
        this.setState({item, text: item.text});
        this.handleNameEdit();
      })
    }
  }

  handleDescriptionEdit = () => {
    this.setState({descriptionEditMode : !this.state.descriptionEditMode})
  }

  handleDescripitonChange = (e) => {
    this.setState({description : e.target.value});
  }

  handleDescriptionKeyPress = (e) => {
    const {item, description} = this.state;
    const {checklistId, itemId} = this.props;
    if (e.key === "Enter") {
      const newItem = Object.assign({...item, description});
      updateItem(checklistId, itemId, newItem).then(checklist => {
        const item = checklist.items.find(item => item._id === itemId);
        this.setState({item, description: item.description});
        this.handleDescriptionEdit();
      })
    }
  }

  render() {
      const {text} = this.state || "";
      const {description} = this.state || "";
      const {nameEditMode, descriptionEditMode} = this.state;
      return(
          <div>
            <input
              value="<--"
              type="button"
              onClick={this.backToChecklist}
            />
          <div>
            {nameEditMode ?
              <input type="text" value={text}
                      onKeyPress={this.handleNameKeyPress}
                      onChange={this.handleNameChange}/>
                    :<div onClick={this.handleNameEdit}>{text}</div>
            }
          </div>
          <br/>
          <div>
            Description
            {descriptionEditMode || !description?
              <input type="text" value={description}
                      onKeyPress={this.handleDescriptionKeyPress}
                      onChange={this.handleDescripitonChange}/>
                    :<div onClick={this.handleDescriptionEdit}>{description}</div>
            }
          </div>
          </div>
      )
  }

}
