import React, { Component } from "react";
import ChecklistListing from "./ChecklistListing";
import { listChecklists } from "./api-utils";

class ChecklistContainer extends Component {
  state = { checklists: [] };

  componentDidMount() {
    listChecklists().then(checklists => {
      this.setState({checklists})
    });
  }

  viewChecklist = ({_id}) => {
    this.props.updateShow("view", _id);
  };

  handleAdd = () => {
    this.props.updateShow("create");
  };

  render() {
    console.log("State", this.state);
    const { checklists } = this.state;
    return (
      <div>
        <div className = "checklist-container">
          {checklists.map(checklist => {
            return (
              <ChecklistListing
                key={checklist._id}
                {...checklist}
                viewChecklist={() => this.viewChecklist(checklist)}
              />
            );
          })}
        </div>
        <br />
        <input className="checklist-container__input" value="+" type="button" onClick={this.handleAdd} />
      </div>
    );
  }
}

export default ChecklistContainer;
