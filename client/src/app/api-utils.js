const checklistUrl = "/api/checklists";

export const listChecklists = () => {
  return fetch(checklistUrl)
    .then(response => response.json());
}

export const getChecklist = (checklistId) => {
  return fetch(`${checklistUrl}/${checklistId}`)
    .then(response => response.json());
}

export const addItem = ({_id}, newItem) => {
  const body = JSON.stringify({ text: newItem });
  return fetch(`${checklistUrl}/${_id}/items`, {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json"
    },
    body
  })
  .then(response => response.json())
}

export const addChecklist = (title) => {
  const body = JSON.stringify({ name: title });
  return fetch(checklistUrl, {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json"
    },
    body
  })
  .then(response => response.json());
}

export const updateItem = (checklistId, itemId, updatedItem) => {
  const body = JSON.stringify(updatedItem);
  return fetch(`${checklistUrl}/${checklistId}/items/${itemId}`, {
    method: "PUT",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json"
    },
    body
  })
  .then(response => response.json())
}
