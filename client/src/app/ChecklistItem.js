import React from 'react';

const ChecklistItem = (props) => {
  const {item, handleCheck, handleItemClick} = props;
  return (
    <li className="checklist__items">
      <input
        type="checkbox"
        checked={item.checked}
        onChange={() => handleCheck(item.id)}
      />
      <div onClick={() => handleItemClick(item.id)}>
        {item.text}
      </div>
    </li>
  )
}

export default ChecklistItem;
