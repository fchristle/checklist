import React from 'react';

const ChecklistListing = (props) => {
  return (
    <div key={props.id} className="checklist-listing" onClick={props.viewChecklist}>
      {props.name}
    </div>
  );
}

export default ChecklistListing;
