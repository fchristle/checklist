import React, { Component } from "react";
import { getChecklist, addItem, updateItem } from "./api-utils";
import ChecklistItem from "./ChecklistItem";

export default class Checklist extends Component {
  state = { checklist: null, newItem: "" };

  componentDidMount() {
    const { checklistId } = this.props;
    getChecklist(checklistId).then(checklist => this.setState({checklist}));
  }

  handleKeyPress = e => {
    const { checklist, newItem } = this.state;
    if (e.key === "Enter") {
      addItem(checklist, newItem).then(checklist => {
        this.setState({ checklist, newItem: "" });
      });
    }
  };

  handleInputChange = e => {
    this.setState({ newItem: e.target.value });
  };

  handleCheck = itemId => {
    const { checklist } = this.state;
    const item = checklist.items.find(i => i._id === itemId);
    updateItem(checklist._id, itemId, { checked: !item.checked })
      .then(checklist => this.setState({checklist}));
  };

  handleItemClick = (itemId) => {
    this.props.updateShow("itemDetails", this.props.checklistId, itemId);
  }

  handleBackToChecklist = () => {
    this.props.updateShow("view", this.props.checklistId);
  }

  handleBack = () => {
    this.props.updateShow("list");
  }

  render() {
    const { checklist, newItem } = this.state;
    if(!checklist) {
      return "";
    }
    return (
      <div className = "checklist"> 
        <input
          value="<--"
          type="button"
          onClick={this.handleBack}
        />
        <ChecklistName name={checklist.name}/>
        <ul>
          {checklist.items.filter(item => !item.checked)
            .map(item => (
            <ChecklistItem key={item._id} item={item}
              handleItemClick={() => this.handleItemClick(item._id)}
              handleCheck={() => this.handleCheck(item._id)}/>
          ))}
          {checklist.items.filter(item => item.checked)
            .map(item => (
            <ChecklistCheckedItem key={item._id} item={item}
              handleCheck={() => this.handleCheck(item._id)}/>
          ))}
        </ul>
        <AddNewItem handleKeyPress={this.handleKeyPress}
                      handleInputChange={this.handleInputChange}
                      newItem={newItem}/>
      </div>
    );
  }
}

const ChecklistName = (props) => (
    <div className="checklist__name">
      {props.name}
    </div>
);

const AddNewItem = (props) => (
  <div className = "add-item">
    <input
      type="text"
      name="newItem"
      placeholder="Add Item"
      onKeyPress={props.handleKeyPress}
      onChange={props.handleInputChange}
      value={props.newItem}
      className = "add-item__input"
    />
  </div>
)

const ChecklistCheckedItem = (props) => {
  const {item, handleCheck} = props;
  return (
    <li className="checklist__items checklist__items--checked">
      <input
        type="checkbox"
        checked={item.checked}
        onChange={() => handleCheck(item.id)}
      />
      <label>
        {item.text}
      </label>
    </li>
  )
}
