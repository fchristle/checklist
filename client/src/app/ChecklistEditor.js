import React, {Component} from 'react';
import {addChecklist} from './api-utils';

class ChecklistEditor extends Component {
  state = {title: ''};

  handleTitleChange = (e) => {
    this.setState({title: e.target.value});
  }

  handleKeyPress = (e) => {
    const {title} = this.state;
    if(e.key === "Enter") {
      addChecklist(title).then(({_id}) => {
        console.log("Added checklist", _id);
        this.props.updateShow("view", _id);
      });
    }
  }

  render() {
    return (
      <div className="checklist-editor"><input type = "text" name = "title"
              placeholder = "Enter name"
              onChange = {this.handleTitleChange}
              onKeyPress = {this.handleKeyPress}
              className="checklist-editor__input"/></div>

    )
  }
}
export default ChecklistEditor;
