const initial = [1, 4, 2, 8, 11];

function get_random(floor, ceiling) {
	floor = Math.floor(floor);
  ceiling = Math.ceil(ceiling);
  return Math.floor(Math.random()* ceiling - floor + 1) + floor;
}
function shuffle(inputArray){
 inputArray.forEach((item, i) => {
 	const j = get_random(i, inputArray.length - 1);
  console.log(j);
  [inputArray[j], inputArray[i]] = [inputArray[i], inputArray[j]]
 });
 console.log(inputArray);
}

shuffle(initial);


function isReproducible(l, n) {
	const asciiCharCount = new Array(256).fill(0);
	[...n].forEach((c, i) => {
		asciiCharCount[n.charCodeAt(i)]++;
	});
	for(let i = 0; i < l.length; i++){
		if(asciiCharCount[l.charCodeAt(i)] === 0) {
			return false;
		}
		asciiCharCount[l.charCodeAt(i)]--;
	}
	return true;
}

function isSingleRiffle(h1, h2, shuffledDeck) {
	let h1Index = 0;
	let h2Index = 0;
	for(card : shuffledDeck) {
		if (card != h1[h1Index++] && card != h2[h2Index++]) {
			return false;
		}
	}
	return true;
}

function LinkedList() {
	this.head = null;
}

function Node(data) {
	this.data = data;
	this.next = null;
}

LinkedList.prototype.add = (val) => {
	let node = new Node(val);
	if(!this.head) {
		this.head = node;
		return node;
	}
	let currNode = this.head;
	while(currNode.next) {
		currNode = currNode.next;
	}
	currNode.next = node;
	return node;
}

function kthToLastNode(k, head) {
	let kNode, curr = head;
	for(i = 0; i < k; i++){
		curr = curr.next;
		if(!curr) {
			return head;
		}
	}
	let lastNode = curr;
	while(lastNode.next != null) {
		kNode = kNode.next;
		lastNode = lastNode.next;
	}
	return kNode;
}



//Prints [1, 3, 4, 5, 6, 8, 10, 11, 12, 14, 15, 19]
function mergeLists(a, b) {
	let aIndex, bIndex = 0;
	let merged = [];
	while(aIndex < a.length && bIndex < b.length ) {
		if(a[aIndex] < b[bIndex]) {
			merged.push(a[aIndex++])
		} else {
			merged.push(b[bIndex++]);
		}
	}
	console.log("Before concat", merged);
	if(aIndex < a.length) {
		merged = merged.concat(a.slice(aIndex));
	} else if(bIndex < b.length) {
		merged = merged.concat(b.slice(bIndex));
	}
}

let my_list     = [3, 4, 6, 10, 11, 15];
let alices_list = [1, 5, 8, 12, 14, 19];
const merged = mergeLists(my_list, alices_list);
console.log(merged);
