const express = require('express');
const fs = require('fs');
const app = express();
const bodyParser = require('body-parser')
const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/checklistdb');
const Schema = mongoose.Schema;
const checklistSchema = new Schema({
  name: String,
  items: [{
    id: Number,
    text: String,
    checked: {type: Boolean, default: false}
  }]
});
const Checklist = mongoose.model('Checklist', checklistSchema);

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

const checklistUrl = "/api/checklists";

app.get('/', (req, res) => {
  res.send("Hello world");
});


app.get(checklistUrl, (req, res) => {
  const checklists = [];
  Checklist.find((err, dbChecklists) => {
    if(err) {
      throw err;
    }
    console.log("checklists db", dbChecklists)
    checklists.push(...dbChecklists);
    console.log("checklists", JSON.stringify(checklists));
    res.send(JSON.stringify(checklists));
  });
});

app.post(checklistUrl, (req, res) => {
  console.log("Creating checklist")
  const {name} = req.body;
  const newChecklist = Checklist({name, items: []});
  console.log(newChecklist);
  newChecklist.save((err, checklist) => {
    if (err){
      return res.status(300).send(err, checklist);
    }
    console.log("new checklist", checklist );
    res.send(JSON.stringify(checklist));
  });
});

app.get(`${checklistUrl}/:id`, (req, res) => {
  console.log("Get by Id")
  const id = req.params.id;
  Checklist.findById(id, (err, checklist) => {
    if(err) {
      throw err;
    }
    res.send(JSON.stringify(checklist));
  });
});

app.post(`${checklistUrl}/:id/items`, (req, res) => {
  console.log("Create new item")
  const id = req.params.id;
  const {text} = req.body;
  Checklist.findByIdAndUpdate(id
    , { $push: {items: {text, checked: false}}}
    , {new: true}
    ,(err, checklist) => {
      if(err) {
        throw err;
      }
      console.log("New item checklist", checklist);
      res.send(JSON.stringify(checklist));
    });
});

app.put(`${checklistUrl}/:id/items/:itemId`, (req, res) => {
  const id = req.params.id;
  const itemId = req.params.itemId;
  const updatedItem = req.body;
  Checklist.findOneAndUpdate({'items._id': itemId}
    , {$set: {'items.$': updatedItem}}
    , {new: true}
    , (err, checklist) => {
        if(err) {
          throw err;
        }
        console.log("Updated item checklist", checklist);
        res.send(JSON.stringify(checklist));
      });
});

const server = app.listen(7000, () => {
  console.log("Server running at http://localhost:" + server.address().port);
});
